package example.myapplication.viewmodel;


import android.databinding.BaseObservable;
import android.databinding.Bindable;

import example.myapplication.Utils;
import example.myapplication.model.FeedsModel;
import io.realm.OrderedRealmCollection;

public class PostsViewModel  extends BaseObservable{

    private FeedsModel feedsModel;

    public PostsViewModel(FeedsModel feedsModel1)
    {
        feedsModel=feedsModel1;
    }

    @Bindable
    public String getTitle()
    {
        return (!Utils.isEmpty(feedsModel.getTitle()))?feedsModel.getTitle():"";

    }

    @Bindable
    public String getBody()
    {
        return (!Utils.isEmpty(feedsModel.getBody()))?feedsModel.getBody():"";
    }
}
