package example.myapplication.model;

import io.realm.RealmList;
import io.realm.RealmObject;


public class DBModel extends RealmObject {

    private RealmList<FeedsModel> feedsModels;

    public void setFeedsModels(RealmList<FeedsModel> feedsModels) {
        this.feedsModels = feedsModels;
    }

    public RealmList<FeedsModel> getFeedsModels() {
        return feedsModels;
    }
}
