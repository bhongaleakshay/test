package example.myapplication;

import android.app.Application;
import android.util.Log;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class TestApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

        RealmConfiguration realmConfiguration=new RealmConfiguration.Builder().name("TestDB").build();

        Log.d("DB",""+realmConfiguration.getRealmFileName());

        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
