package example.myapplication.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.myapplication.R;
import example.myapplication.Utils;
import example.myapplication.databinding.AdapterPostsBinding;
import example.myapplication.model.FeedsModel;
import example.myapplication.viewmodel.PostsViewModel;
import io.realm.OrderedRealmCollection;


public class  PostAdapter extends  RecyclerView.Adapter<PostAdapter.PostViewHolder> {


    private OrderedRealmCollection<FeedsModel> feedsModels;
    private Context mContext;
    public PostAdapter(Context context,OrderedRealmCollection<FeedsModel> feedsModels1)
    {
        mContext=context;
        feedsModels=feedsModels1;
    }
    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_posts,parent,false);

        AdapterPostsBinding adapterPostsBinding=DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_posts, parent, false);
        return  new PostViewHolder(adapterPostsBinding);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        FeedsModel feedsModel=feedsModels.get(position);


        /*if(!Utils.isEmpty(feedsModel.getTitle()))
        {
            holder.txtTitle.setText(feedsModel.getTitle());
        }

        if(!Utils.isEmpty(feedsModel.getBody()))
        {
            holder.txtDescription.setText(feedsModel.getBody());
        }*/

        AdapterPostsBinding binding = holder.adapterPostsBinding;
        binding.setPosts(new PostsViewModel(feedsModel));

    }

    public void updateList(OrderedRealmCollection<FeedsModel> feedsModels1)
    {
        this.feedsModels=feedsModels1;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return feedsModels.size();
    }

   class PostViewHolder extends RecyclerView.ViewHolder
    {

        AdapterPostsBinding adapterPostsBinding;

        public PostViewHolder(AdapterPostsBinding adapterPostsBinding) {

            super(adapterPostsBinding.cardView);
            this.adapterPostsBinding=adapterPostsBinding;
        }
    }

}
