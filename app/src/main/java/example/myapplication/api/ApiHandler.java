package example.myapplication.api;


public interface ApiHandler<T>
{
    void onSuccess(T response);
    void onError(String message);
}