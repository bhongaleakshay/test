package example.myapplication.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.util.ArrayList;

import example.myapplication.Utils;
import example.myapplication.model.FeedsModel;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class ApiCall {

        public static ApiCall apiCall;
        public static API api;
        public static Gson gson;
        public static String BASE_URL="http://jsonplaceholder.typicode.com/";


        public static ApiCall getApiCall()
        {
            if(apiCall==null)
            {
                apiCall=new ApiCall();
            }
            return apiCall;
        }

        public API getAPI()
        {
            if (api == null)
            {
                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier
                        .STATIC);
                gsonBuilder.excludeFieldsWithoutExposeAnnotation();
                gson = gsonBuilder.create();
                HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                httpClient.addInterceptor(loggingInterceptor);

                Retrofit.Builder retrofitBuilder = new Retrofit.Builder();

                retrofitBuilder.baseUrl(BASE_URL);
                retrofitBuilder.addCallAdapterFactory(RxJavaCallAdapterFactory.create());
                retrofitBuilder.addConverterFactory(GsonConverterFactory.create(gson));
                retrofitBuilder.client(httpClient.build());
                Retrofit retrofit = retrofitBuilder.build();
                api = retrofit.create(API.class);
            }

            return api;
        }


   /* public void getPosts(final ApiHandler<ArrayList<FeedsModel>> apiHandler)
    {
        Call<ArrayList<FeedsModel>> call=getAPI().getPosts();

        call.enqueue(new Callback<ArrayList<FeedsModel>>() {
            @Override
            public void onResponse(Call<ArrayList<FeedsModel>> call, Response<ArrayList<FeedsModel>> response) {
                if(response!=null && response.isSuccessful())
                {
                    apiHandler.onSuccess(response.body());
                }
                else
                {
                    apiHandler.onError("No record found");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FeedsModel>> call, Throwable t) {
                if(t instanceof IOException)
                {
                    apiHandler.onError(Constants.NO_INTERNET_CONNECTION);
                }
                else {
                    apiHandler.onError(!Utils.isEmpty(t.getLocalizedMessage())?t.getLocalizedMessage(): "Please  try again");
                }
            }
        });

    }*/

    public void getPosts(final ApiHandler<ArrayList<FeedsModel>> apiHandler)
    {
        rx.Observable<ArrayList<FeedsModel>> feeds=getAPI().getRxPosts();

        feeds.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<ArrayList<FeedsModel>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                apiHandler.onError((!Utils.isEmpty(e.getLocalizedMessage()))?e.getLocalizedMessage(): "Please  try again");
            }

            @Override
            public void onNext(ArrayList<FeedsModel> feedsModels) {
                apiHandler.onSuccess(feedsModels);
            }
        });

    }

}
