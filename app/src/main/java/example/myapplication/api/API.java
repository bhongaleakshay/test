package example.myapplication.api;

import java.util.ArrayList;

import example.myapplication.model.FeedsModel;
import retrofit2.Call;
import retrofit2.http.GET;


public interface API
{

    String POSTS="posts";

    @GET(POSTS)
    Call<ArrayList<FeedsModel>>     getPosts();

    @GET(POSTS)
    rx.Observable<ArrayList<FeedsModel>> getRxPosts();
}
