package example.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import example.myapplication.adapter.PostAdapter;
import example.myapplication.api.ApiCall;
import example.myapplication.api.ApiHandler;
import example.myapplication.model.FeedsModel;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    Unbinder unbinder;

    PostAdapter postAdapter;

    OrderedRealmCollection<FeedsModel> feedsModels;

    ApiCall apiCall;

    Realm realm=Realm.getDefaultInstance();

    RealmChangeListener<RealmResults<FeedsModel>> changeListener;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        unbinder= ButterKnife.bind(this);

        apiCall=ApiCall.getApiCall();




        recyclerView.setLayoutManager(new LinearLayoutManager(this));



        setDBListner();


    }

    private void setDBListner()
    {
        RealmResults<FeedsModel> feedsModels= realm.where(FeedsModel.class).findAllAsync();

        changeListener=new RealmChangeListener<RealmResults<FeedsModel>>() {
            @Override
            public void onChange(RealmResults<FeedsModel> element) {
                if(element!=null && element.size()>0) {
                    if(postAdapter!=null) {
                        postAdapter.updateList(element);
                    }
                    else
                    {
                        postAdapter=new PostAdapter(MainActivity.this,element);
                        recyclerView.setAdapter(postAdapter);
                    }
                }
                else
                {
                   checkConnection();
                }

            }
        };
        feedsModels.addChangeListener(changeListener);
    }

    private void checkConnection()
    {
        if(Utils.isInternetConnected(MainActivity.this))
        {
            makeAPiCall();
        }
        else
        {
            Utils.showSimpleAlertView(MainActivity.this,Constants.NO_INTERNET_CONNECTION,"oks");
        }
    }
    private void makeAPiCall()
    {
        progressBar.setVisibility(View.VISIBLE);

        apiCall.getPosts(new ApiHandler<ArrayList<FeedsModel>>()
        {
            @Override
            public void onSuccess(final ArrayList<FeedsModel> response)
            {
                progressBar.setVisibility(View.INVISIBLE);

                Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
                    @Override
                       public void execute(Realm realm) {

                        for(FeedsModel model:response)
                        {
                            FeedsModel details=realm.createObject(FeedsModel.class);
                            details.setBody(model.getBody());
                            details.setId(model.getId());
                            details.setTitle(model.getTitle());
                            details.setUseId(model.getUseId());
                        }

                    }
                });


            }

            @Override
            public void onError(String message)
            {
                progressBar.setVisibility(View.INVISIBLE);

                Utils.showSimpleAlertView(MainActivity.this,message,"ok");
            }
        });
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(unbinder!=null)
        {
            unbinder.unbind();
        }
    }
}
